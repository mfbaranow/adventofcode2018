import Foundation
extension Int: Error { }
func day1() {
    let nums = try! String(contentsOfFile: "input_1a.txt")
        .split(separator: "\n")
        .map { Int(String($0))! }
    print("day1a answer: \(nums.reduce(0, +))")

    var found = Set<Int>()
    do {
        _ = try (0...).lazy
            .map { nums[$0 % nums.count] }
            .reduce(0) { (a, b) throws -> Int in
                let f = a + b
                if found.contains(f) { throw f }
                found.insert(f)
                return f
            }
    } catch {
        print("day1b answer: \(error)")
    }
}



