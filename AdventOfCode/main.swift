//
//  main.swift
//  AdventOfCode
//
//  Created by Matt Baranowski on 12/3/18.
//  Copyright © 2018 mfbaranow. All rights reserved.
//

import Foundation

// https://adventofcode.com/2018/leaderboard/private/view/122791

//measureTime(day1)
//measureTime(day2a)
//measureTime(day2b)
//measureTime(day3a)
//measureTime(day3b)
//measureTime(day4)
measureTime(day5a)
measureTime(day5b)

