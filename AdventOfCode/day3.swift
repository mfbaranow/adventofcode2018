//
//  day3.swift
//  AdventOfCode
//
//  Created by Matt Baranowski on 12/3/18.
//  Copyright © 2018 mfbaranow. All rights reserved.
//

import Foundation

struct Square {
    let id: Int
    let x: Int
    let y: Int
    let w: Int
    let h: Int
    
    static let size = 1000
    static func makeFabric() -> [[Int]] {
        return Array(repeating: Array(repeating: 0, count: size), count: size)
    }
    
    static func readInput() -> [Square] {
        let lines = try! String(contentsOfFile: "input_3.txt").split(separator: "\n")
        let seperators = CharacterSet(charactersIn: " #@,:x")
        var squares: [Square] = []
        for line in lines {
            let nums = line.split { seperators.contains($0.unicodeScalars.first!) }.map { Int($0)! }
            let s = Square(id: nums[0], x: nums[1], y: nums[2], w: nums[3], h: nums[4])
            squares.append(s)
        }
        return squares
    }
}

func day3a() {
    let squares = Square.readInput()
    var fabric = Square.makeFabric()
    
    for s in squares {
        for y in s.y..<(s.y + s.h) {
        for x in s.x..<(s.x + s.w) {
            fabric[y][x] = fabric[y][x] + 1
        }}
    }
    
    var numOverlaps = 0
    for y in 0..<Square.size {
    for x in 0..<Square.size {
        if fabric[y][x] > 1 {
            numOverlaps += 1
        }
    }}
    
    print("day3a answer: \(numOverlaps)")
}

func day3b() {
    let squares = Square.readInput()
    var fabric = Square.makeFabric()

    var nonOverlapping = Set<Int>()
    squares.forEach { nonOverlapping.insert($0.id) }
    
    for s in squares {
        for y in s.y..<(s.y + s.h) {
        for x in s.x..<(s.x + s.w) {
            let otherId = fabric[y][x]
            if otherId != 0 {
                nonOverlapping.remove(s.id)
                nonOverlapping.remove(otherId)
            }
            fabric[y][x] = s.id
        }}
    }
    
    print("day3b answer: \(nonOverlapping)")
}
