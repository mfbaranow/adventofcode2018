//
//  day5.swift
//  AdventOfCode
//
//  Created by Matt Baranowski on 12/5/18.
//  Copyright © 2018 mfbaranow. All rights reserved.
//

import Foundation

func reduce(polymer input: String, toRemove: String) -> Int {
    var polymer = input
    var canReduce = true
    var iterations = 0
    var reductions = 0
    while canReduce {
        iterations += 1
        var newPolymer = ""
        
        let p1 = polymer.unicodeScalars
        var p2 = polymer.unicodeScalars
        p2.removeFirst()
        canReduce = false
        var skipNext = false
        for (a,b) in zip(p1, p2) {
            if skipNext || String(a).lowercased() == toRemove {
                skipNext = false
                continue
            }
            if a != b, String(a).lowercased() == String(b).lowercased() {
                reductions += 1
                canReduce = true
                skipNext = true
                continue
            }
            newPolymer += String(a)
        }
        if !skipNext, let last = polymer.last {
            newPolymer.append(last)
        }
        
        polymer = newPolymer
    }
    
    //print("iterations \(iterations) reductions \(reductions)")
    return polymer.count
}

func day5a() {
    var polymer = try! String(contentsOfFile: "input_5.txt")
    polymer.removeLast()
    print("day5a answer: \(reduce(polymer: polymer, toRemove: "-"))")
}

extension ClosedRange where Bound == String {
    var values: ClosedRange<UInt32> { return  lowerBound.unicodeScalars.first!.value...upperBound.unicodeScalars.first!.value }
    var unicodeScalars: [UnicodeScalar] { return values.compactMap(UnicodeScalar.init) }
    var characters: [Character] { return unicodeScalars.map(Character.init) }
    var string: String { return String(String.UnicodeScalarView(unicodeScalars)) }
}

func day5b() {
    var polymer = try! String(contentsOfFile: "input_5.txt")
    polymer.removeLast()

    
    let range = "a"..."z"
    var minCount = polymer.count
    for c in range.unicodeScalars  {
        var testPolymer = polymer
        var set = CharacterSet(charactersIn: "\(c)\(String(c).uppercased())")
        testPolymer.removeAll { set.contains($0.unicodeScalars.first!) }
        let count = reduce(polymer: polymer, toRemove: String(c))
        if count < minCount {
            minCount = count
        }
    }
    
    print("day5a answer: \(minCount)")
}
