//
//  day4.swift
//  AdventOfCode
//
//  Created by Matt Baranowski on 12/4/18.
//  Copyright © 2018 mfbaranow. All rights reserved.
//

import Foundation

struct Event {
    let date: String
    let time: String
    let event: String
}

struct Sleep {
    let id: Int
    let start: Int
    let end: Int
}

func day4() {
    let lines = try! String(contentsOfFile: "input_4.txt").split(separator: "\n")

    var events: [Event] = []
    for l in lines {
        var parts = l.split(separator: " ", maxSplits: 2).map { String($0) }
        events.append(Event(date: parts[0], time: parts[1], event: parts[2]))
    }
    
    let sortedEvents = events.sorted { (a, b) -> Bool in
        return "\(a.date) \(a.time)" < "\(b.date) \(b.time)"
    }
    
    let timeToInt = { (str: String) -> Int in
        var s = String(str.split(separator: ":")[1])
        s.removeLast()
        return Int(s)!
    }
    
    var naps: [Sleep] = []
    var lastId: Int = 0
    var fallAsleep: Int = 0
    for event in sortedEvents {
        if event.event.hasSuffix("begins shift") {
            let parts = event.event.split(separator: " ")
            var id = String(parts[1])
            id.removeFirst()
            lastId = Int(id)!
        }
        
        else if event.event == "falls asleep" {
            fallAsleep = timeToInt(event.time)
        }
        
        else if event.event == "wakes up" {
            let wakeUp = timeToInt(event.time)
            naps.append(Sleep(id: lastId, start: fallAsleep, end: wakeUp))
        }
    }
    
    var guardNapTime: [Int: Int] = [:]
    for s in naps {
        let timeSlept = (s.end - s.start)
        assert(timeSlept > 0)
        guardNapTime[s.id] = guardNapTime[s.id, default: 0] + timeSlept
    }
    
    let maxGuard = guardNapTime.max { $0.value < $1.value }!
    
    var minutes: [Int] = Array(repeating: 0, count: 60)
    for s in naps {
        guard s.id == maxGuard.key else {
            continue
        }

        for m in s.start..<s.end {
            minutes[m] += 1
        }
    }
    
    let maxMinute = minutes.enumerated().max { $0.1 < $1.1 }!
    
    let answer = maxGuard.key * maxMinute.offset
    print("day4a answer \(answer)")
    
    
    var guardMinutes: [Int: [Int]] = [:]
    var maxGuardMinutes = 0
    var maxGuardMinuteIdx = 0
    var maxGuardMinuteId: Int = 0
    for s in naps {
        var minutes = guardMinutes[s.id, default: Array(repeating: 0, count: 60)]
        for m in s.start..<s.end {
            minutes[m] += 1
            if minutes[m] > maxGuardMinutes {
                maxGuardMinutes = minutes[m]
                maxGuardMinuteIdx = m
                maxGuardMinuteId = s.id
            }
        }
        
        guardMinutes[s.id] = minutes
    }
    
    print("day4b answer \(maxGuardMinuteId * maxGuardMinuteIdx)")
}

