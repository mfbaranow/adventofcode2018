//
//  utility.swift
//  AdventOfCode
//
//  Created by Matt Baranowski on 12/2/18.
//  Copyright © 2018 mfbaranow. All rights reserved.
//

import Foundation

func measureTime(_ block: () -> Void) {
    let start = Date()
    block()
    let end = Date()
    let interval = end.timeIntervalSince(start)
    print("... in \(interval) seconds")
}
