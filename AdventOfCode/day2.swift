import Foundation

func day2a() {
    let ids = try! String(contentsOfFile: "input_2.txt").split(separator: "\n")
    
    func letterCounts(_ s: String) -> Set<Int> {
        var m: [Character:Int] = [:]
        for c in s {
            m[c] = m[c, default:0] + 1
        }
        var c = Set<Int>()
        for (_,value) in m {
            c.insert(value)
        }
        return c
    }

    var numTriple = 0
    var numDouble = 0
    ids.forEach {
        let c = letterCounts(String($0))
        numDouble += c.contains(2) ? 1 : 0
        numTriple += c.contains(3) ? 1 : 0
    }

    print("day2a answer: \(numDouble * numTriple)")
}


func day2b() {
    let ids = try! String(contentsOfFile: "input_2.txt").split(separator: "\n")

    let minLen = ids.map { $0.count }.min() ?? 0
    for p in 0..<minLen {
        let idsWithoutLetter = ids.map { a -> String in
            var s = a
            s.remove(at: s.index(s.startIndex, offsetBy: p))
            return String(s)
        }.sorted()
        
        var found = Set<String>()
        for id in idsWithoutLetter {
            if found.contains(id) {
                print("day2b answer: \(id)")
                break
            }
            found.insert(id)
        }
    }
}
